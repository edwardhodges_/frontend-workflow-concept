// /* eslint strict: 0, dot-notation: 0, no-console: 0 */

// // *************************************
// //
// //   Gulpfile
// //
// // *************************************

// var gulp = require('gulp');
// var plugins = require('gulp-load-plugins')();

// // -------------------------------------
// //   Options
// // -------------------------------------
// var options = {

//     // Default
//     default: {
//         tasks: ['js:all', 'css:lib', 'css:main', 'watch']
//     },

//     // Build
//     build: {
//         tasks: ['js:all', 'css:lib', 'css:main']
//     },

//     // CSS
//     css: {
//         destination: 'dist/css',
//         files: {
//             lib: 'scss/lib/lib.scss',
//             main: 'scss/main.scss',
//             finalMain: function() {
//                 return options.css.destination + '/main.css';
//             }
//         },
//         lint: ['scss/**/*.scss', '!scss/lib/*.scss'],
//         prefixes: ['> 2%']
//     },

//     // HTML
//     html: {
//         files: '*.html'
//     },

//     // JavaScript
//     js: {
//         destination: 'dist/js',
//         files: ['js/lib/*.js', 'js/main.js', 'js/**/*.js'],
//         lint: ['Gulpfile.js', 'js/**/*.js', '!js/lib/*.js'] // Exclude all scripts inside js/lib/
//     },

//     lint: {
//         errors: {}, // Object to store lint errors
//         tasks: {
//             all: ['lint:html', 'lint:js', 'lint:sass']
//         }
//     },

//     hooks: {
//         'pre-commit': ['lint:js:hook', 'lint:sass:hook']
//     },

//     // Watch
//     watch: function() {
//         return [

//             // Watch JS
//             {
//                 files: options.js.lint,
//                 tasks: ['js:all']
//             },

//             // Watch Sass
//             {
//                 files: options.css.lint,
//                 tasks: ['css:main']
//             }
//         ];
//     }
// };

// // -------------------------------------
// //   Task: Default
// // -------------------------------------

// gulp.task('default', options.default.tasks);

// // -------------------------------------
// //   Task: Build
// // -------------------------------------

// gulp.task('build', options.build.tasks);

// // -------------------------------------
// //   Task: Generate CSS - Lib
// // -------------------------------------

// gulp.task('css:lib', function() {
//     return plugins.rubySass(options.css.files.lib, { style: 'expanded' })
//         .pipe(gulp.dest(options.css.destination))
//         .pipe(plugins.rename({ suffix: '.min' }))
//         .pipe(plugins.minifyCss())
//         .pipe(gulp.dest(options.css.destination));
// });

// // -------------------------------------
// //   Task: Compile Sass - Main
// // -------------------------------------

// gulp.task('compile:sass:main', function() {
//     return plugins.rubySass(options.css.files.main, {
//             style: 'expanded',
//             sourcemap: true
//         })
//         .pipe(plugins.autoprefixer({
//             browsers: options.css.prefixes
//         }))
//         .pipe(plugins.sourcemaps.write('/'))
//         .pipe(gulp.dest(options.css.destination));
// });

// // -------------------------------------
// //   Task: Gerenrate CSS - Main
// // -------------------------------------

// gulp.task('css:main', ['lint:sass', 'compile:sass:main'], function() {
//     return gulp.src(options.css.files.finalMain())
//         .pipe(plugins.rename({ suffix: '.min' }))
//         .pipe(plugins.minifyCss())
//         .pipe(gulp.dest(options.css.destination));
// });

// // -------------------------------------
// //  Task: Lint Sass - Main
// // -------------------------------------

// function lintSass(isHook) {
//     return gulp.src(options.css.lint)
//         .pipe(plugins.scssLint({
//             config: '.scss-lint.yml',
//             maxBuffer: 9999999
//         }))
//         .pipe(plugins.if(isHook, plugins.scssLint.failReporter()));
// }

// gulp.task('lint:sass', function() {
//     return lintSass();
// });

// // -------------------------------------
// //  Task: Lint Sass - Main for Git Hook
// // -------------------------------------

// gulp.task('lint:sass:hook', function() {
//     return lintSass(true);
// });

// // -------------------------------------
// //   Task: Generate JS (lib + Main)
// // -------------------------------------

// gulp.task('js:all', ['lint:js'], function() {
//     var _dest = options.js.destination;

//     // If JS lint has errors don't build JS
//     if (options.lint.errors.js > 0) return false;

//     return gulp.src(options.js.files)
//         .pipe(plugins.concat('main.js'))
//         .pipe(gulp.dest(_dest))
//         .pipe(plugins.uglify())
//         .pipe(plugins.rename({ suffix: '.min' }))
//         .pipe(gulp.dest(_dest));
// });

// // -------------------------------------
// //   Task: Lint JS
// // -------------------------------------

// function lintJs(isHook) {
//     return gulp.src(options.js.lint)
//         .pipe(plugins.eslint())
//         .pipe(plugins.eslint.format())
//         .pipe(plugins.eslint.results(function(results) {
//             options.lint.errors.js = results.errorCount;
//         }))
//         .pipe(plugins.if(isHook, plugins.eslint.failAfterError()));
// }

// gulp.task('lint:js', function() {
//     return lintJs();
// });

// // -------------------------------------
// //   Task: Lint JS for Git Hook
// // -------------------------------------

// gulp.task('lint:js:hook', function() {
//     return lintJs(true);
// });

// // -------------------------------------
// //   Task: Lint HTML
// // -------------------------------------

// gulp.task('lint:html', function() {
//     return gulp.src(options.html.files)
//         .pipe(plugins.htmlhint('.htmlhintrc'))
//         .pipe(plugins.htmlhint.reporter());
// });

// // -------------------------------------
// //   Task: Lint All (HTML, JS and Sass)
// // -------------------------------------

// gulp.task('lint:all', options.lint.tasks.all);

// // -------------------------------------
// //   Task: Watch
// // -------------------------------------

// gulp.task('watch', function() {
//     var watchFiles = options.watch();

//     watchFiles.forEach(function(watching) {
//         gulp.watch(watching.files, watching.tasks);
//     });
// });

// // -------------------------------------
// //   Task: Git Hooks
// // -------------------------------------

// gulp.task('pre-commit', ['lint:js:hook', 'lint:sass:hook']);