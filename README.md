# Front-end Workflow Concept

## TODO
* Setup
    * Fork & Sync
    * Clone
    * NPM install
* Gulp tasks
    * Watchers
    * JS linter
    * SASS linter
    * Git hooks
* NPM tasks
    * Node server
    * Build new component
* SASS
    * Bourbon, Neat
* Templates
    * Handlebars
* Prototyping
    * Bitters
* Production tasks
    * Compile JS
    * Compile CSS
    * Compile HTML
